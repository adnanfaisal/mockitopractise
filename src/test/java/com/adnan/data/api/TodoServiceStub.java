package com.adnan.data.api;

import java.util.Arrays;
import java.util.List;

public class TodoServiceStub implements TodoService {

	// a dummy implementation of the method
	public List<String> retrieveTodos(String user) {
		return Arrays.asList("Learn Mockito", "Learn Spring", "Learn Spring MVC");
	}

	public boolean deleteTodo(String toDo) {
		
		List<String> todos = Arrays.asList("Learn Mockito", "Learn Spring", "Learn Spring MVC");
		return todos.remove(toDo);
	}
	
	

}
