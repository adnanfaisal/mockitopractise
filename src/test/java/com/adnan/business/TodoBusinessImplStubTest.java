package com.adnan.business;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.Test;

import com.adnan.business.TodoBusinessImpl;
import com.adnan.data.api.TodoServiceStub;


public class TodoBusinessImplStubTest {

	@Test // naming convention test+functionInTest
	public void testRetrieveTodosRelatedToSpring_usingAStub() {
		TodoServiceStub todoServiceStub = new TodoServiceStub();
		TodoBusinessImpl todoBusinessImpl = new  TodoBusinessImpl(todoServiceStub); 
		List<String> filteredTodo = todoBusinessImpl.retrieveTodosRelatedToSpring("adnan");
	
		assertEquals(2,filteredTodo.size());
	
	}

}
