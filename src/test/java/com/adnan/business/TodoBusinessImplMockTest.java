package com.adnan.business;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.*;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.then;
import static org.mockito.Mockito.atLeast;
import static org.mockito.Mockito.atLeastOnce;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Arrays;
import java.util.List;

import org.junit.Test;
import org.mockito.ArgumentCaptor;

import com.adnan.business.TodoBusinessImpl;
import com.adnan.data.api.TodoService;
import com.adnan.data.api.TodoServiceStub;


public class TodoBusinessImplMockTest {

	@Test // naming convention test+functionInTest
	public void testRetrieveTodosRelatedToSpring_usingAMock() {
		
		// declare a mock object. mock() can create instances of interface or class.
		TodoService todoServiceMock = mock(TodoService.class);
		
		//add return values for input parameter "Dummy"	
		List<String> todoList = Arrays.asList("Learn Mockito", "Learn Spring", "Learn Spring MVC");
		when(todoServiceMock.retrieveTodos("Dummy")).thenReturn(todoList);
		
		TodoBusinessImpl todoBusinessImpl = new  TodoBusinessImpl(todoServiceMock); 
		List<String> filteredTodo = todoBusinessImpl.retrieveTodosRelatedToSpring("Dummy");
	
		assertEquals(2,filteredTodo.size());
	
	}
	
	@Test 
	public void testRetrieveTodosRelatedToSpring_usingBDD() {
		
		//GIVEN
			TodoService todoServiceMock = mock(TodoService.class);
			List<String> todoList = Arrays.asList("Learn Mockito", "Learn Spring", "Learn Spring MVC");
			
			given(todoServiceMock.retrieveTodos("Dummy")).willReturn(todoList);
			// actually this given-willReturn works exactly the same as when-then we
			// saw in other methods. Mockito changed these names to align with BDD.

			TodoBusinessImpl todoBusinessImpl = new  TodoBusinessImpl(todoServiceMock); 

		//WHEN
			List<String> filteredTodo = todoBusinessImpl.retrieveTodosRelatedToSpring("Dummy");
		
		//THEN
		assertThat(filteredTodo.size(),is(2)); // note the use of That and is given by BDDMockito
	
	}
	
	// verify on calls are practised in this method
	@Test 
	public void testDeleteTodosNotRelatedToSpring_usingBDD_verify() {
		
		//GIVEN
			TodoService todoServiceMock = mock(TodoService.class);
			List<String> todoList = Arrays.asList("Learn Mockito", "Learn Spring", "Learn Swimming","Learn Swimming");
			
			given(todoServiceMock.retrieveTodos("Dummy")).willReturn(todoList);
			// actually this given-willReturn works exactly the same as when-then we
			// saw in other methods. Mockito changed these names to align with BDD.

			TodoBusinessImpl todoBusinessImpl = new  TodoBusinessImpl(todoServiceMock); 

		//WHEN
			todoBusinessImpl.deleteTodosNotRelatedToSpring("Dummy");
		
		/*
		 * * Verify if todoBusinessImpl.deleteTodosNotRelatedToSpring() is called 
		 * * then todoServiceMock.deleteTodo() is called
		 */
		//THEN
			verify(todoServiceMock).deleteTodo("Learn Mockito");
			
			/* test if deleteTodo() is NOT called */
			verify(todoServiceMock,never()).deleteTodo("Learn Spring");
			
			/* test if deleteTodo() is called twice */
			verify(todoServiceMock,times(2)).deleteTodo("Learn Swimming");
			
			/* test if deleteTodo() is called at  least once */
			verify(todoServiceMock,atLeastOnce()).deleteTodo("Learn Swimming");
			
			/* test if deleteTodo() is called at  least 2 times */
			verify(todoServiceMock,atLeast(2)).deleteTodo("Learn Swimming");
			
			
	}

	// verify on calls are practised in this method
		@Test 
		public void testDeleteTodosNotRelatedToSpring_usingBDD_thenShould() {
			
			//GIVEN
				TodoService todoServiceMock = mock(TodoService.class);
				List<String> todoList = Arrays.asList("Learn Mockito", "Learn Spring", "Learn Swimming","Learn Swimming");
				
				given(todoServiceMock.retrieveTodos("Dummy")).willReturn(todoList);
				// actually this given-willReturn works exactly the same as when-then we
				// saw in other methods. Mockito changed these names to align with BDD.

				TodoBusinessImpl todoBusinessImpl = new  TodoBusinessImpl(todoServiceMock); 

			//WHEN
				todoBusinessImpl.deleteTodosNotRelatedToSpring("Dummy");
			
			/*
			 * * Verify if todoBusinessImpl.deleteTodosNotRelatedToSpring() is called 
			 * * then todoServiceMock.deleteTodo() is called
			 */
			//THEN
				
				then(todoServiceMock).should().deleteTodo("Learn Mockito");
				
				/* test if deleteTodo() is NOT called */
				then(todoServiceMock).should(never()).deleteTodo("Learn Spring");
				
				/* test if deleteTodo() is called twice */
				then(todoServiceMock).should(times(2)).deleteTodo("Learn Swimming");
				
				/* test if deleteTodo() is called at  least once */
				then(todoServiceMock).should(atLeastOnce()).deleteTodo("Learn Swimming");
				
				/* test if deleteTodo() is called at  least 2 times */
				then(todoServiceMock).should(atLeast(2)).deleteTodo("Learn Swimming");
								
		}

		// argument capture practise
		@Test 
		public void testDeleteTodosNotRelatedToSpring_usingBDD_argumentCapture() {
			
			// Step 1: Declare Argument Captor
			ArgumentCaptor<String> stringArgumentCaptor = ArgumentCaptor.forClass(String.class); 
			
			
			
			//GIVEN
				TodoService todoServiceMock = mock(TodoService.class);
				List<String> todoList = Arrays.asList("Learn Mockito", "Learn Spring");
				
				given(todoServiceMock.retrieveTodos("Dummy")).willReturn(todoList);
				// actually this given-willReturn works exactly the same as when-then we
				// saw in other methods. Mockito changed these names to align with BDD.

				TodoBusinessImpl todoBusinessImpl = new  TodoBusinessImpl(todoServiceMock); 

			//WHEN
				todoBusinessImpl.deleteTodosNotRelatedToSpring("Dummy");
			
			
			//THEN | Step 2: Capture the argument		
				then(todoServiceMock).should().deleteTodo(stringArgumentCaptor.capture());
				
				//Step 3: assert
				assertThat(stringArgumentCaptor.getValue(), is("Learn Mockito"));
								
		}
		
		// argument capture practise
				@Test 
				public void testDeleteTodosNotRelatedToSpring_usingBDD_argumentCaptureMultipleTimes() {
					
					// Step 1: Declare Argument Captor
					ArgumentCaptor<String> stringArgumentCaptor = ArgumentCaptor.forClass(String.class); 
					
					
					
					//GIVEN
						TodoService todoServiceMock = mock(TodoService.class);
						List<String> todoList = Arrays.asList("Learn Mockito", "Learn Spring","Learn Swimming");
						
						given(todoServiceMock.retrieveTodos("Dummy")).willReturn(todoList);
						// actually this given-willReturn works exactly the same as when-then we
						// saw in other methods. Mockito changed these names to align with BDD.

						TodoBusinessImpl todoBusinessImpl = new  TodoBusinessImpl(todoServiceMock); 

					//WHEN
						todoBusinessImpl.deleteTodosNotRelatedToSpring("Dummy");
					
					
					//THEN | Step 2: Capture multiple arguments		
						then(todoServiceMock).should(times(2)).deleteTodo(stringArgumentCaptor.capture());
						
						//Step 3: assert
						assertThat(stringArgumentCaptor.getAllValues().size() , is(2));
										
				}


}
