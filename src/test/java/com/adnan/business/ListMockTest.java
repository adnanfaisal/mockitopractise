package com.adnan.business;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.*;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.mockito.Mockito.anyInt;

import org.junit.Test;

import java.util.Arrays;
import java.util.List;

public class ListMockTest {

	@SuppressWarnings("rawtypes")
	List listMock;

	public ListMockTest(){
		listMock = mock(List.class);
	}
	@Test
	public void testListSize(){

		
		/*We want list.size() to return 2*/
		when(listMock.size()).thenReturn(2); //set it up		
		assertEquals(2,listMock.size()); // test
		assertEquals(2,listMock.size()); // test again
		
		
		/*We want list.size() to return Multiple values. 
		 * First time to return 10, then 5. 
		 */
		when(listMock.size()).thenReturn(10).thenReturn(5); //set it up
		assertEquals(10,listMock.size()); // test
		assertEquals(5,listMock.size()); // test again
		assertEquals(5,listMock.size()); // test again
		
	}
	
	@Test
	public void testListGet(){
		when(listMock.get(0)).thenReturn("hello");
		assertEquals("hello",listMock.get(0));
		
		// "nice mock" behaviour of mockito.
		assertEquals(null,listMock.get(1)); //  EasyMock does not have such behaviour, that would have thrown error
		
		// Argument Matcher
		when(listMock.get(anyInt())).thenReturn("hello");
		assertEquals("hello",listMock.get(0));
		assertEquals("hello",listMock.get(5));		
	}
	
	@Test
	public void testListGetBDD(){
		
		//GIVEN
		given(listMock.get(0)).willReturn("hello");
		
		//WHEN
		String s = (String) listMock.get(0);
		
		//THEN
		assertThat(s,is("hello"));
	}
	
	@Test(expected = RuntimeException.class)
	public void testListException(){
		when(listMock.get(anyInt())).thenThrow(new RuntimeException("Something"));
		listMock.get(0);
	}
	
	@Test
	public void testListMixArgumentMatcher(){
		
		// 0 and 3 both fixed numbers
		when(listMock.subList(0,3)).thenReturn(Arrays.asList(5,10,15));
		assertEquals(new Integer(10), (Integer) listMock.subList(0,3).get(1));
		
		// mixing fixed no. with Argument Matcher won't work.
		// Throws "InvalidUseOfMatchersException"
//		when(listMock.subList(anyInt(),3)).thenReturn(Arrays.asList(5,10,15));
//		assertEquals(new Integer(10), (Integer) listMock.subList(0,3).get(1));
		
	}
}
